<?php

error_reporting(E_ALL);
ini_set('display_startup_errors', '1');
ini_set('display_errors', '1');

require_once '../autoload.php';

define('APP_NAMESPACE', 'OptimaApps');

function plog($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

function globalExceptionHandler($exception)
{
    echo '<h2>Exception</h2>';
    plog(explode(',', $exception->getMessage()));
    plog($exception->getTraceAsString());
}

set_exception_handler('globalExceptionHandler');

use Core\Router\SimpleRouter;
use Core\DependencyInjection\DependencyContainer;
use Core\View\InvalidViewException;
use Core\Router\RouteDefinition;

$routes = require_once '../'.BASE_APP_DIR.'/'.APP_NAMESPACE.'/config/routing.php';

$homeRoute = RouteDefinition::instance(RouteDefinition::GET)->setControllerClass(GUEST_BOOK_CONTROLLER)
                                                            ->setControllerMethod('homePage');

$router = new SimpleRouter();
$router->setRoutes($routes);
$router->setHomeRoute($homeRoute);

if (false === strpos(php_sapi_name(), 'cli')) {
    $router->processHTTP($_SERVER, $_GET, $_POST);
} else {
    $router->processCLI($argv);
}

$matchedRoute = $router->getMatchedRoute();

$controllerClass = $matchedRoute->getControllerClass();
$controllerMethod = $matchedRoute->getControllerMethod();

$controller = new $controllerClass();

$baseServicesConfig = require_once '../'.BASE_APP_DIR.'Core/config/services.php';
$appServicesConfig = require_once '../'.BASE_APP_DIR.'/'.APP_NAMESPACE.'/config/services.php';
$databaseConfig = require_once '../'.BASE_APP_DIR.'/'.APP_NAMESPACE.'/config/database.php';

$container = new DependencyContainer();

$allServices = array_merge($baseServicesConfig, $appServicesConfig);

foreach ($allServices as $key => $value) {
    if (false === strpos($key, '@')) {
        $container->registerService($key, $value);
    } else {
        $container->setProperty(ltrim($key, '@'), $value);
    }
}

$container->setService('dependency_container', $container);
$container->setProperty('database_config', $databaseConfig);

$controller->setDependencyContainer($container);

$variableSlugs = $router->getVariableSlugs();

if ($variableSlugs && count($variableSlugs) > 0) {
    $view = call_user_func_array(array($controller, $controllerMethod), $variableSlugs);
} else {
    $view = $controller->$controllerMethod();
}

if ($view) {
    $view->render();
} else {
    throw new InvalidViewException('Controller returned invalid view.');
}
