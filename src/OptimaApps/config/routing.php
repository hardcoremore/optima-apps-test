<?php

use Core\Router\RouteDefinition;

const GUEST_BOOK_ROUTE = 'guestbook';
const GUEST_BOOK_CONTROLLER = 'OptimaApps\Controller\GuestBookController';

return [
    RouteDefinition::instance([RouteDefinition::GET, RouteDefinition::POST])->addSlug(GUEST_BOOK_ROUTE)
                                                   ->addSlug('create')
                                                   ->setControllerClass(GUEST_BOOK_CONTROLLER)
                                                   ->setControllerMethod('create'),

    RouteDefinition::instance(RouteDefinition::GET)->addSlug(GUEST_BOOK_ROUTE)
                                                   ->addSlug('get')
                                                   ->addSlug('id', false, RouteDefinition::DIGIT_ONLY)
                                                   ->setControllerClass(GUEST_BOOK_CONTROLLER)
                                                   ->setControllerMethod('get'),

    RouteDefinition::instance([RouteDefinition::GET,  RouteDefinition::POST])->addSlug(GUEST_BOOK_ROUTE)
                                                   ->addSlug('edit')
                                                   ->addSlug('id', false, RouteDefinition::DIGIT_ONLY)
                                                   ->setControllerClass(GUEST_BOOK_CONTROLLER)
                                                   ->setControllerMethod('edit'),

    RouteDefinition::instance(RouteDefinition::DELETE)->addSlug(GUEST_BOOK_ROUTE)
                                                      ->addSlug('delete')
                                                      ->addSlug('id', false, RouteDefinition::DIGIT_ONLY)
                                                      ->setControllerClass(GUEST_BOOK_CONTROLLER)
                                                      ->setControllerMethod('deleteAjax'),

    RouteDefinition::instance(RouteDefinition::GET)->addSlug(GUEST_BOOK_ROUTE)
                                                   ->addSlug('node')
                                                   ->addSlug('nodeId', false, RouteDefinition::DIGIT_ONLY)
                                                   ->setControllerClass(GUEST_BOOK_CONTROLLER)
                                                   ->setControllerMethod('fetchAjaxTreeNode'),
];
