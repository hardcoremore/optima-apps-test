<?php

use Core\DependencyInjection\ServiceDefinition;

return [
    'table_gateway_factory' => new ServiceDefinition(
        'OptimaApps\Factory\GuestBookTableGatewayFactory',
        null,
        [
            'setDependencyContainer' => '@dependency_container',
        ]
    ),

    'model_factory' => new ServiceDefinition(
        'OptimaApps\Factory\ModelFactory',
        null,
        [
            'setTableGatewayFactory' => '@table_gateway_factory',
        ]
    ),

    'template_factory' => new ServiceDefinition('OptimaApps\Factory\GuestBookTemplateFactory'),
];
