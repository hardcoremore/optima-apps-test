<?php

namespace OptimaApps\Model;

use Core\BaseModel;

class UserModel extends BaseModel
{
    public function getUserData()
    {
        return [
            'firstName' => 'Časlav',
            'lastName' => 'Šabani',
            'email' => 'caslav.sabani@gmail.com',
        ];
    }
}
