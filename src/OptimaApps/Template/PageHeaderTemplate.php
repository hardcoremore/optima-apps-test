<?php

namespace OptimaApps\Template;

use Core\Template\BaseHtmlTemplate;

class PageHeaderTemplate extends BaseHtmlTemplate
{
    private $template = <<<EOF
    <h2>User: <span style="color:purple">{{firstName}} {{lastName}}<span></h2>
EOF;

    public function getTemplate()
    {
        return $this->template;
    }
}
