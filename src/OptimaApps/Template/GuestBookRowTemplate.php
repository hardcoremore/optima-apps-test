<?php

namespace OptimaApps\Template;

use Core\Template\BaseHtmlTemplate;

class GuestBookRowTemplate extends BaseHtmlTemplate
{
    private $template = <<<EOF
    <tr>
        <td>{{id}}</td>
        <td>{{name}}</td>
        <td>{{title}}</td>
        <td>{{comment}}</td>
        <td>{{email}}</td>
        <td>
            <a href="{{guestBookUpdateUrl}}/{{id}}">Edit</a>
            <a href="{{guestBookDeleteUrl}}/{{id}}">Delete</a>
        </td>
    </tr>
EOF;

    public function getTemplate()
    {
        return $this->template;
    }
}
