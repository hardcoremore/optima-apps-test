<?php

namespace OptimaApps\Template;

use Core\Template\FormTemplate;
use Core\Factory\BaseTemplateFactory;

class GuestBookFormTemplate extends FormTemplate
{
    public function __construct(BaseTemplateFactory $templateFactory)
    {
        parent::__construct($templateFactory);

        $this->setProperty('nameSpace', 'guest-book-form');
    }
}
