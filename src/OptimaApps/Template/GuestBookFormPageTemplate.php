<?php

namespace OptimaApps\Template;

use Core\Template\BaseHtmlTemplate;
use OptimaApps\TemplateTraits\HeaderTemplateData;

class GuestBookFormPageTemplate extends BaseHtmlTemplate
{
    use HeaderTemplateData;

    private $successTemplate = <<<EOF
    <h4>New Guest Book is created</h4>
    <h3 style="color: green">{{successMessage}}</h3>
    <a href="{{createGuestBookUrl}}">Create new Guest Book</a>
    <a href="{{guestBookHomePageUrl}}">Return To Home Page</a>
EOF;

    private $errorTemplate = <<<EOF
    <h4>Creating Guest Book Failed :(</h4>
    <h3 style="color: red">{{errorMessage}}</h3>
EOF;

    private $formTemplate = <<<EOF
    <h2>Create new Guest Book form</h2>
    <div class="form-container">
        {%form%}
    </div>
EOF;

    public function getTemplate()
    {
        if ($this->getProperty('errorMessage')) {
            return $this->errorTemplate.$this->formTemplate;
        } elseif ($this->getProperty('successMessage')) {
            return $this->successTemplate;
        } else {
            return $this->formTemplate;
        }
    }

    private $cancelButtonUrl;

    private $submitUrl = '';

    private $formData = [];

    public function setCancelButtonUrl(string $url)
    {
        $this->cancelButtonUrl = $url;
    }

    public function setSubmitUrl(string $url)
    {
        $this->submitUrl = $url;
    }

    public function setFormData(array $data)
    {
        $this->formData = $data;
    }

    public function getFormData():array
    {
        return $this->formData;
    }

    public function compile()
    {
        $formTemplate = $this->templateFactory->get('guest_book_form');

        $formInputs = [];

        $input = $this->templateFactory->get('text_input_form_item');
        $input->setProperty('label', 'Name');
        $input->setProperty('placeHolder', 'Name');
        $input->setProperty('name', 'name');
        $input->setProperty('value', $this->formData['name'] ?? '');
        $input->compile();

        $formInputs[] = $input;

        $input = $this->templateFactory->get('text_input_form_item');
        $input->setProperty('label', 'Title');
        $input->setProperty('placeHolder', 'Title');
        $input->setProperty('name', 'title');
        $input->setProperty('value', $this->formData['title'] ?? '');

        $formInputs[] = $input;

        $input = $this->templateFactory->get('text_area_input_form_item');
        $input->setProperty('label', 'Comment');
        $input->setProperty('placeHolder', 'Comment');
        $input->setProperty('name', 'comment');
        $input->setProperty('value', $this->formData['comment'] ?? '');

        $formInputs[] = $input;

        $input = $this->templateFactory->get('text_input_form_item');
        $input->setProperty('label', 'Email');
        $input->setProperty('placeHolder', 'Email');
        $input->setProperty('name', 'email');
        $input->setProperty('value', $this->formData['email'] ?? '');

        $formInputs[] = $input;

        $formTemplate->addChildTemplate('formInputs', $formInputs);

        $this->addChildTemplate('form', $formTemplate);

        // child template needds to be compiled
        $formTemplate->compile();
        

        $pageTemplate = $this->templateFactory->get('base_page');
        $pageTemplate->setHeaderData($this->getHeaderData());

        $main = $pageTemplate->compile();
        $main->addChildTemplate('pageContent', $this);

        return $main;
    }
}
