<?php

namespace OptimaApps\Template;

use Core\Template\BaseHtmlTemplate;

class PageFooterTemplate extends BaseHtmlTemplate
{
    private $template = <<<EOF
    <h2>Optima Apps Footer</h2>
    <a href="/aboutUs">About Us</a>
    <a href="/terms">Terms And Conditions</a>
EOF;

    public function getTemplate()
    {
        return $this->template;
    }
}
