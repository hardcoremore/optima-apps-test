<?php

namespace OptimaApps\Template;

use OptimaApps\TemplateTraits\HeaderTemplateData;
use Core\Template\BaseHtmlTemplate;

class GuestBookHomePageTemplate extends BaseHtmlTemplate
{
    use HeaderTemplateData;

    private $template = <<<EOF
<h1 style="color:blue">Welcome to Guest Book Page</h1>

<a href="{{createGuestBookUrl}}"><h3 style="color:green">Create New Guest Book Entry</h2></a>

<div>
    <table>
        <tr>
           <th>Id</th>
           <th>Name</th>
           <th>Title</th>
           <th>Comment</th>
           <th>Email</th>
           <th>Controls</th>
        </tr>
        {%guestBookRows%}
    </table>
</div>

EOF;

    private $guestBookData;

    public function setGuestBookData($data)
    {
        $this->guestBookData = $data;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function compile()
    {
        $rows = [];

        foreach ($this->guestBookData as $value) {
            $rowTemplate = $this->templateFactory->get('guest_book_row');
            $rowTemplate->setProperties($value);

            $rows[] = $rowTemplate;
        }

        $this->addChildTemplate('guestBookRows', $rows);

        $pageTemplate = $this->templateFactory->get('base_page');
        $pageTemplate->setHeaderData($this->getHeaderData());

        $main = $pageTemplate->compile();
        $main->addChildTemplate('pageContent', $this);

        return $main;
    }
}
