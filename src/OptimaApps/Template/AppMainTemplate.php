<?php

namespace OptimaApps\Template;

use Core\Template\BaseHtmlTemplate;

class AppMainTemplate extends BaseHtmlTemplate
{
    private $template = <<<EOF
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Optima Apps Guest Book</title>
</head>

<body class="test-123">
{%mainLayout%}
</body>

</html>
EOF;

    public function getTemplate()
    {
        return $this->template;
    }
}
