<?php

namespace OptimaApps\Template;

use Core\Template\BaseHtmlTemplate;
use OptimaApps\TemplateTraits\HeaderTemplateData;

class BasePageTemplate extends BaseHtmlTemplate
{
    use HeaderTemplateData;

    protected $pageContentData;

    public function compile()
    {
        $main = $this->templateFactory->get('app_main_layout');

        $mainLayout = $main->compile()->getChildTemplate('mainLayout');

        $mainLayout->getChildTemplate('header')->setProperties($this->headerData);

        return $main;
    }
}
