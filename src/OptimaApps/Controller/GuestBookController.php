<?php

namespace OptimaApps\Controller;

use Core\BaseController;

class GuestBookController extends BaseController
{
    public function homePage()
    {
        $userData = $this->getModel('user')->getUserData();

        $template = $this->getTemplate('guest_book_home_page');
        $template->setHeaderData($userData);
        $template->setProperty('createGuestBookUrl', '/guestbook/create');

        $template->setGuestBookData($this->getModel('guest_book')->readAll());

        $template->setProperty('guestBookUpdateUrl', '/guestbook/edit');
        $template->setProperty('guestBookDeleteUrl', '/guestbook/delete');

        return $this->getView('html')->setTemplate($template);
    }

    public function create()
    {
        $userData = $this->getModel('user')->getUserData();

        $template = $this->getTemplate('guest_book_form_page');
        $template->setHeaderData($userData);

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            try {
                $guestBookId = $this->getModel('guest_book')->create($_POST);
                $template->setProperty('successMessage', 'Guest Book with id: '.$guestBookId.' created!');
                $template->setProperty('createGuestBookUrl', '/guestbook/create');
                $template->setProperty('guestBookHomePageUrl', '/');
            } catch (\Exception $error) {
                $template->setProperty('errorMessage', $error->getMessage());
            }
        } else {
            $template->setPublicProperty('actionUrl', '/guestbook/create');
            $template->setPublicProperty('formMethod', 'post');
            $template->setPublicProperty('cancelFormButtonUrl', '/');
        }

        return $this->getView('html')->setTemplate($template);
    }

    public function edit($id)
    {
        $userData = $this->getModel('user')->getUserData();

        $template = $this->getTemplate('guest_book_form_page');
        $template->setHeaderData($userData);

        $guestBookData = $this->getModel('guest_book')->getById($id);

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            try {
                $guestBookId = $this->getModel('guest_book')->update($id, $_POST);
                $template->setProperty('successMessage', 'Guest Book with id: '.$guestBookId.' created!');
                $template->setProperty('createGuestBookUrl', '/guestbook/create');
                $template->setProperty('guestBookHomePageUrl', '/');
            } catch (\Exception $error) {
                $template->setProperty('errorMessage', $error->getMessage());
            }
        } else {
            $template->setFormData($guestBookData);
            $template->setPublicProperty('actionUrl', '/guestbook/edit/'.$id);
            $template->setPublicProperty('formMethod', 'post');
            $template->setPublicProperty('cancelFormButtonUrl', '/');
        }

        return $this->getView('html')->setTemplate($template);
    }

    public function get($id)
    {
        $userData = $this->getModel('user')->getUserData();

        $template = $this->getTemplate('guest_book_home_page');
        $template->setHeaderData($userData);
        $template->setProperty('createGuestBookUrl', '/guestbook/create');

        $selectedGuestBook = $this->getModel('guest_book')->readAll();
        $countryData = $this->getModel('guest_book')->readAll();

        return $this->getView('html')->setTemplate($template);
    }

    public function deleteAjax($id)
    {
        $guestBookModel = $this->getModel('guest_book');

        $selectedGuestBook = $guestBookModel->getById($id);

        if($selectedGuestBook) {
            $result = $guestBookModel->delete($id);

            if($result) {
                return $this->getView('json')->setViewData(
                    'You have deleted Guest Book With Id: ' . $id
                );
            }
            else {
                return $this->getView('json')->setViewData(
                    'Error ocurred while deleting Guest Book With Id: ' . $id
                );
            }
        }
        else {
            return $this->getView('json')->setViewData(
                'Guest Book With Id: ' . $id . ' does not exists'
            );
        }
    }

    public function fetchAllCountriesAjax($entryId)
    {
        return $this->getView('json')->setViewData(
            $this->getModel('guest_book')->getNodeChildren($entryId)
        );
    }
}
