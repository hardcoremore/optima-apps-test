<?php

namespace OptimaApps\Database;

use Core\Database\BaseTableGateway;

class GuestBookTable extends BaseTableGateway
{
    public function __construct()
    {
        $this->columns = [
            'id' => [
                'type' => 'int',
            ],
            'name' => [
                'type' => 'string',
                'required' => true,
            ],
            'title' => [
                'type' => 'string',
                'required' => true,
            ],
            'comment' => [
                'type' => 'string',
                'required' => true,
            ],
            'email' => [
                'type' => 'string',
                'required' => false,
            ],
        ];
    }
}
