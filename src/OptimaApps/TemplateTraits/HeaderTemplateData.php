<?php

namespace OptimaApps\TemplateTraits;

trait HeaderTemplateData
{
    protected $headerData;

    public function setHeaderData(array $data)
    {
        $this->headerData = $data;
    }

    public function getHeaderData()
    {
        return $this->headerData;
    }
}
