<?php

namespace OptimaApps\Factory;

use Core\Factory\BaseCamelCaseFactory;
use OptimaApps\Model\UserModel;
use OptimaApps\Model\GuestBookModel;

class ModelFactory extends BaseCamelCaseFactory
{
    private $tableGatewayFactory;

    public function __construct()
    {
        $this->methodPostfix = 'Model';
    }

    public function setTableGatewayFactory(GuestBookTableGatewayFactory $factory)
    {
        $this->tableGatewayFactory = $factory;
    }

    public function getTableGatewayFactory()
    {
        return $this->tableGatewayFactory;
    }

    public function createUserModel()
    {
        return new UserModel();
    }

    public function createGuestBookModel()
    {
        $table = $this->tableGatewayFactory->createGuestBookTable();

        $model = new GuestBookModel();
        $model->setTable($table);

        return $model;
    }
}
