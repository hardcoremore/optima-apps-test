<?php

namespace OptimaApps\Factory;

use Core\Factory\BaseTemplateFactory;
use OptimaApps\Template\AppMainTemplate;
use OptimaApps\Template\PageHeaderTemplate;
use OptimaApps\Template\PageFooterTemplate;
use OptimaApps\Template\AppMainLayoutTemplate;
use OptimaApps\Template\BasePageTemplate;
use OptimaApps\Template\GuestBookHomePageTemplate;
use OptimaApps\Template\GuestBookFormPageTemplate;
use OptimaApps\Template\GuestBookFormTemplate;
use OptimaApps\Template\GuestBookRowTemplate;

class GuestBookTemplateFactory extends BaseTemplateFactory
{
    public function createAppMainTemplate()
    {
        return new AppMainTemplate($this);
    }

    public function createPageHeaderTemplate()
    {
        return new PageHeaderTemplate($this);
    }

    public function createPageFooterTemplate()
    {
        return new PageFooterTemplate($this);
    }

    public function createBasePageTemplate()
    {
        return new BasePageTemplate($this);
    }

    public function createAppMainLayoutTemplate()
    {
        return new AppMainLayoutTemplate($this);
    }

    public function createGuestBookHomePageTemplate()
    {
        return new GuestBookHomePageTemplate($this);
    }

    public function createGuestBookFormTemplate()
    {
        return new GuestBookFormTemplate($this);
    }

    public function createGuestBookFormPageTemplate()
    {
        return new GuestBookFormPageTemplate($this);
    }

    public function createGuestBookRowTemplate()
    {
        return new GuestBookRowTemplate($this);
    }
}
