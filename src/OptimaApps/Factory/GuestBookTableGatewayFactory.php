<?php

namespace OptimaApps\Factory;

use Core\Factory\BaseTableGatewayFactory;
use OptimaApps\Database\GuestBookTable;

class GuestBookTableGatewayFactory extends BaseTableGatewayFactory
{
    public function createGuestBookTable()
    {
        $connection = $this->createDatabaseConnection();

        $table = new GuestBookTable();
        $table->setTableName('guestbook');
        $table->setIdColumnName('id');
        $table->setConnection($connection);

        return $table;
    }
}
