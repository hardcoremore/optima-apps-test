<?php

namespace Core\Router;

class SimpleRouter
{
    protected $homeRoute;
    protected $routes;

    protected $lastMatchedRoute;
    protected $variableSlugs;

    public function setRoutes($routes)
    {
        $this->routes = $routes;
    }

    public function setHomeRoute(RouteDefinition $homeRoute)
    {
        $this->homeRoute = $homeRoute;
    }

    public function getMatchedRoute()
    {
        return $this->lastMatchedRoute;
    }

    public function getVariableSlugs()
    {
        return $this->variableSlugs;
    }

    public function processHTTP(array $serverData, array $queryData, array $postData)
    {
        $requestUri = $serverData['REQUEST_URI'];
        $httpMethod = $serverData['REQUEST_METHOD'];

        if (0 === strlen($requestUri) || '/' === $requestUri) {
            $this->setHomeRouteAsMatched();
        } else {
            $this->lastMatchedRoute = null;
            $this->variableSlugs = [];

            if (0 === strpos($requestUri, '/')) {
                $requestUri = ltrim($requestUri, '/');
            }

            $allUrlSlugs = explode('/', $requestUri);
            $urlSlugCount = count($allUrlSlugs);

            if ($urlSlugCount > 0) {
                $isRouteMatched = false;

                for ($c = 0, $clen = count($this->routes); $c < $clen; ++$c) {
                    $routeDefinition = $this->routes[$c];

                    // check if it is the same http method type
                    if (is_array($routeDefinition->getType())) {
                        if (false === array_search($httpMethod, $routeDefinition->getType(), true)) {
                            continue;
                        }
                    } elseif ($routeDefinition->getType() !== $httpMethod) {
                        continue;
                    }

                    // check that there are more required slugs than all slug count
                    if ($urlSlugCount < $routeDefinition->getRequiredSlugCount()) {
                        continue;
                    }

                    for ($i = 0; $i < $urlSlugCount; ++$i) {
                        $isRouteMatched = false;

                        $urlSlug = $allUrlSlugs[$i];

                        $slugDefinition = $routeDefinition->getSlugAt($i);

                        if ($slugDefinition) {
                            if ($slugDefinition->patternMatch) {
                                $isSlugMatched = 1 === preg_match($slugDefinition->patternMatch, $urlSlug);

                                if ($isSlugMatched) {
                                    $this->variableSlugs[] = $urlSlug;
                                }
                            } else {
                                $isSlugMatched = $slugDefinition->name === $urlSlug;
                            }

                            if (!$isSlugMatched && $slugDefinition->isOptional) {
                                $isSlugMatched = true;
                            }

                            $isRouteMatched = $isSlugMatched;
                        } else {
                            // no slug definition is found at current index. route is not matched
                            $isSlugMatched = false;
                        }

                        $isRouteMatched = $isSlugMatched;

                        if (!$isSlugMatched) {
                            break;
                        }
                    }

                    if ($isRouteMatched) {
                        $this->lastMatchedRoute = $routeDefinition;
                        break;
                    }
                }
            }

            if (!$this->lastMatchedRoute) {
                throw new InvalidHomeRouteException('No route is matched.');
            }
        }
    }

    protected function setHomeRouteAsMatched()
    {
        if (null == $this->homeRoute) {
            throw new InvalidHomeRouteException('No route is matched and no default route is set either.');
        } else {
            $this->lastMatchedRoute = $this->homeRoute;
        }
    }

    public function processCLI(array $optionsData)
    {
    }
}
