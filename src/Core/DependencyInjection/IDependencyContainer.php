<?php

namespace Core\DependencyInjection;

interface IDependencyContainer
{
    public function getServiceDefinition($serviceName);

    public function registerService($serviceName, ServiceDefinition $service);

    public function getService($serviceName, $newInstance = false);
}
