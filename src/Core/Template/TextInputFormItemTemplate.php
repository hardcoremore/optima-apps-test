<?php

namespace Core\Template;

use Core\Factory\BaseTemplateFactory;

class TextInputFormItemTemplate extends BaseHtmlTemplate
{
    private $template = <<<EOF
<div class="form-field-set {{class}}">
    <span class="form-field-error form-field-error-{{name}}" data-field-name="{{name}}"></span>
    <label class="form-field-label" for="{{name}}-{{nameSpace}}-input">{{label}}</label>
    <input type="{{type}}" class="form-field-input" name="{{name}}" id="{{name}}-{{nameSpace}}-input" placeholder="{{placeHolder}}" value="{{value}}" />
</div>
EOF;

    public function __construct(BaseTemplateFactory $templateFactory)
    {
        parent::__construct($templateFactory);

        $this->setProperty('type', 'text');
        $this->setProperty('class', 'text-input');
        $this->setProperty('value', '');
    }

    public function getTemplate()
    {
        return $this->template;
    }
}
