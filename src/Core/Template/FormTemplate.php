<?php

namespace Core\Template;

use Core\Factory\BaseTemplateFactory;

class FormTemplate extends BaseHtmlTemplate
{
    private $template = <<<EOF
<form method="{{formMethod}}" action="{{actionUrl}}" id="{{formId}}" class="crud-module-form {{formClass}}">

    {%formInputs%}

    {%formControls%}

</form>
EOF;

    public function __construct(BaseTemplateFactory $templateFactory)
    {
        parent::__construct($templateFactory);

        $this->setProperty('formId', 'form-id');
        $this->setProperty('formClass', 'form-class');

        $this->setProperty('formMethod', 'post');

        $this->setProperty('saveFormButtonLabel', 'Save');
        $this->setProperty('cancelFormButtonLabel', 'Cancel');
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function compile()
    {
        $this->addChildTemplate('formControls', $this->templateFactory->get('form_controls'));
        return $this;
    }
}
