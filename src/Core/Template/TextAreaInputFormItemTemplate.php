<?php

namespace Core\Template;

use Core\Factory\BaseTemplateFactory;

class TextAreaInputFormItemTemplate extends BaseHtmlTemplate
{
    private $template = <<<EOF
<div class="form-field-set {{class}}">
    <span class="form-field-error form-field-error-{{name}}" data-field-name="{{name}}"></span>
    <label class="form-field-label" for="{{name}}-{{nameSpace}}-input">{{label}}</label>
    <textarea id="{{name}}-{{nameSpace}}-input" name="{{name}}" class="form-field-input">{{value}}</textarea>
</div>


EOF;

    public function __construct(BaseTemplateFactory $templateFactory)
    {
        parent::__construct($templateFactory);

        $this->setProperty('class', 'textarea-input');
        $this->setProperty('value', '');
    }

    public function getTemplate()
    {
        return $this->template;
    }
}
