<?php

namespace Core\Template;

use Core\Factory\BaseTemplateFactory;

abstract class BaseHtmlTemplate
{
    protected $templateFactory;

    protected $propertiesList;

    // public properties will be propagated to child templates
    protected $publicPropertiesList;

    protected $templatesList;

    protected $compiledString;

    public function __construct(BaseTemplateFactory $templateFactory)
    {
        $this->templateFactory = $templateFactory;

        $this->templatesList = [];
        $this->propertiesList = [];
        $this->publicPropertiesList = [];

        $this->compiledString = '';
    }

    public function setTemplateFactory(BaseTemplateFactory $factory)
    {
        $this->templateFactory = $factory;
    }

    public function getTemplateFactory()
    {
        return $this->templateFactory;
    }

    public function addChildTemplate($name, $template)
    {
        $this->templatesList[$name] = $template;
    }

    public function getChildTemplate($name)
    {
        if (array_key_exists($name, $this->templatesList)) {
            return $this->templatesList[$name];
        } else {
            return null;
        }
    }

    public function getAllChildTemplates()
    {
        return $this->templatesList;
    }

    public function setProperty($name, $value)
    {
        $this->propertiesList[$name] = $value;

        return $this;
    }

    public function setPublicProperty($name, $value)
    {
        $this->publicPropertiesList[$name] = $value;
        $this->setProperty($name, $value);
        return $this;
    }

    public function setProperties(array $properties)
    {
        foreach ($properties as $key => $value) {
            $this->propertiesList[$key] = $value;
        }

        return $this;
    }

    public function getAllProperties():array
    {
        return $this->propertiesList;
    }

    public function getAllPublicProperties():array
    {
        return $this->publicPropertiesList;
    }

    public function getProperty($name)
    {
        if (array_key_exists($name, $this->propertiesList)) {
            return $this->propertiesList[$name];
        }
        else if(array_key_exists($name, $this->publicPropertiesList)) {
            return $this->publicPropertiesList[$name];
        }
        else {
            return null;
        }
    }

    public function getCompiledString()
    {
        return $this->compiledString;
    }

    public function setCompiledString($compiledString)
    {
        $this->compiledString = $compiledString;
    }

    public function getTemplate()
    {
    }

    public function compile()
    {
    }

    public function toString()
    {
        $finalString = $this->getTemplate();

        foreach ($this->templatesList as $key => $template) {
            $keySearchString = '{%'.$key.'%}';

            if (is_array($template)) {
                $arrayTemplateString = '';

                foreach ($template as $subTemplate) {
                    // propagate all public properties to child template
                    $subTemplate->setProperties($this->publicPropertiesList);
                    $arrayTemplateString .= $subTemplate->toString();
                }

                $finalString = str_replace(
                    $keySearchString,
                    $arrayTemplateString,
                    $finalString
                );
            } else {
                
                // propagate all public properties to child template
                $template->setProperties($this->publicPropertiesList);

                $finalString = str_replace(
                    $keySearchString,
                    $template->toString(),
                    $finalString
                );
            }
        }

        foreach ($this->propertiesList as $key => $value) {
            $propertySearchKey = '{{'.$key.'}}';

            $finalString = str_replace(
                $propertySearchKey,
                htmlspecialchars($value, ENT_QUOTES, 'UTF-8'),
                $finalString
            );
        }

        return $finalString;
    }
}
