<?php

namespace Core\View;

interface IView
{
    public function setViewData($data);

    public function getViewData();

    public function setStatusCode($code);

    public function getStatusCode();

    public function setUserHeaders(array $headers);

    public function getUserHeaders();

    public function getAllHeaders();

    public function getFormattedOutputData();
}
