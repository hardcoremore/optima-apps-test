<?php

namespace Core\Factory;

use Core\View\JsonView;
use Core\View\HtmlView;

class ViewFactory extends BaseCamelCaseFactory
{
    public function __construct()
    {
        $this->methodPostfix = 'View';
    }

    public function createJsonView()
    {
        return new JsonView();
    }

    public function createHtmlView()
    {
        return new HtmlView();
    }
}
