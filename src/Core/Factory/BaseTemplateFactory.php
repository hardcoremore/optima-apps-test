<?php

namespace Core\Factory;

use Core\Template\BaseHtmlTemplate;
use Core\Template\FormTemplate;
use Core\Template\FormControlsTemplate;
use Core\Template\TextInputFormItemTemplate;
use Core\Template\TextAreaInputFormItemTemplate;

class BaseTemplateFactory extends BaseCamelCaseFactory
{
    public function __construct()
    {
        $this->methodPostfix = 'Template';
    }

    public function createHtmlTemplate()
    {
        return new BaseHtmlTemplate();
    }

    public function createFormTemplate()
    {
        return new FormTemplate($this);
    }

    public function createFormControlsTemplate()
    {
        return new FormControlsTemplate($this);
    }

    public function createTextInputFormItemTemplate()
    {
        return new TextInputFormItemTemplate($this);
    }

    public function createTextAreaInputFormItemTemplate()
    {
        return new TextAreaInputFormItemTemplate($this);
    }
}
