<?php

namespace Core\Database;

interface ITableGateway
{
    public function setConnection(\PDO $conn);

    public function getConnection();

    public function setTableName($name);

    public function getTableName();

    public function readAll($delegate = false);

    public function iterate($callback);

    public function getById($id);

    public function getOneBy($columnName, $value);

    public function search(array $params, $delegate = false);

    public function create(array $data);

    public function update($id, array $data);

    public function delete($id);

    public function deleteAll();
}
