<?php

namespace Core;

use Core\Database\ITableGateway;

class BaseModel
{
    protected $table;

    public function setTable(ITableGateway $table)
    {
        $this->table = $table;
    }

    public function getTable()
    {
        return $this->table;
    }

    public function readAll()
    {
        return $this->table->readAll();
    }

    public function getById($id)
    {
        return $this->table->getById($id);
    }

    public function search(array $params)
    {
    }

    public function create(array $data)
    {
        $result = $this->table->create($data);
    }

    public function update($id, array $data)
    {
    }

    public function delete($id)
    {
        return $this->table->delete($id);
    }

    public function deleteAll()
    {
    }
}
