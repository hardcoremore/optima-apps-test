<?php

use Core\DependencyInjection\ServiceDefinition;

return [
    'database_connection_factory' => new ServiceDefinition('Core\Factory\DatabaseConnectionFactory'),

    'template_factory' => new ServiceDefinition('Core\Factory\BaseTemplateFactory'),
    'view_factory' => new ServiceDefinition('Core\Factory\ViewFactory'),
];
